package cz.tedsoft.tipcalculator;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.Wearable;

public class SettingsActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme();
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                    }
                })
                .addApi(Wearable.API)
                .build();
        googleApiClient.connect();

        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);
    }

    private void setTheme() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String themeColor = sharedPrefs.getString("theme_color", "5");
        String theme = sharedPrefs.getString("theme","1");
        switch (themeColor) {
            case "1":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Red_Dark);
                } else {
                    setTheme(R.style.AppTheme_Red);
                }
                break;
            case "2":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Pink_Dark);
                } else {
                    setTheme(R.style.AppTheme_Pink);
                }
                break;
            case "3":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Purple_Dark);
                } else {
                    setTheme(R.style.AppTheme_Purple);
                }
                break;
            case "4":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_DeepPurple_Dark);
                } else {
                    setTheme(R.style.AppTheme_DeepPurple);
                }
                break;
            case "5":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Indigo_Dark);
                }
                break;
            case "6":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Blue_Dark);
                } else {
                    setTheme(R.style.AppTheme_Blue);
                }
                break;
            case "7":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_LightBlue_Dark);
                } else {
                    setTheme(R.style.AppTheme_LightBlue);
                }
                break;
            case "8":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Cyan_Dark);
                } else {
                    setTheme(R.style.AppTheme_Cyan);
                }
                break;
            case "9":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Teal_Dark);
                } else {
                    setTheme(R.style.AppTheme_Teal);
                }
                break;
            case "10":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Green_Dark);
                } else {
                    setTheme(R.style.AppTheme_Green);
                }
                break;
            case "11":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_LightGreen_Dark);
                } else {
                    setTheme(R.style.AppTheme_LightGreen);
                }
                break;
            case "12":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Lime_Dark);
                } else {
                    setTheme(R.style.AppTheme_Lime);
                }
                break;
            case "13":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Yellow_Dark);
                } else {
                    setTheme(R.style.AppTheme_Yellow);
                }
                break;
            case "14":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Amber_Dark);
                } else {
                    setTheme(R.style.AppTheme_Amber);
                }
                break;
            case "15":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Orange_Dark);
                } else {
                    setTheme(R.style.AppTheme_Orange);
                }
                break;
            case "16":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_DeepOrange_Dark);
                } else {
                    setTheme(R.style.AppTheme_DeepOrange);
                }
                break;
            case "17":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Brown_Dark);
                } else {
                    setTheme(R.style.AppTheme_Brown);
                }
                break;
            case "18":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Grey_Dark);
                } else {
                    setTheme(R.style.AppTheme_Grey);
                }
                break;
            case "19":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_BlueGrey_Dark);
                } else {
                    setTheme(R.style.AppTheme_BlueGrey);
                }
                break;
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPrefs, String key) {
        syncDataItems();
    }

    private void syncDataItems () {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean roundUp = sharedPreferences.getBoolean("round_up", true);
        String defaultTip = sharedPreferences.getString("default_tip", "");
        String defaultSplit = sharedPreferences.getString("default_split", "");
        boolean proVersion = sharedPreferences.getBoolean("pro", false);
        String currency = sharedPreferences.getString("currency_value", "");
        String currencyPosition = sharedPreferences.getString("currency_position", "2");

        if(googleApiClient==null)
            return;

        final PutDataMapRequest putRequest = PutDataMapRequest.create("/Wear_settings");
        final DataMap map = putRequest.getDataMap();
        map.putBoolean("round_up", roundUp);
        map.putString("default_tip", defaultTip);
        map.putString("default_split", defaultSplit);
        map.putBoolean("pro", proVersion);
        map.putString("currency_value", currency);
        map.putString("currency_position", currencyPosition);
        Wearable.DataApi.putDataItem(googleApiClient,  putRequest.asPutDataRequest());
    }
}
