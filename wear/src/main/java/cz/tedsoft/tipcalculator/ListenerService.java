package cz.tedsoft.tipcalculator;

import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.List;

public class ListenerService extends WearableListenerService {

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        super.onDataChanged(dataEvents);

        final List<DataEvent> events = FreezableUtils.freezeIterable(dataEvents);
        for(DataEvent event : events) {
            final Uri uri = event.getDataItem().getUri();
            final String path = uri!=null ? uri.getPath() : null;
            if("/Wear_settings".equals(path)) {
                final DataMap map = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();
                // read your values from map:
                boolean roundUp = map.getBoolean("round_up");
                String defaultTip = map.getString("default_tip");
                String defaultSplit = map.getString("default_split");
                boolean proVersion = map.getBoolean("pro");
                String currency = map.getString("currency_value");
                String currencyPosition = map.getString("currency_position");

                SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putBoolean("round_up", roundUp);
                editor.putString("default_tip", defaultTip);
                editor.putString("default_split", defaultSplit);
                editor.putBoolean("pro", proVersion);
                editor.putString("currency_value", currency);
                editor.putString("currency_position", currencyPosition);
                editor.apply();
            }
        }
    }
}

