package cz.tedsoft.tipcalculator;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class AboutActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView aboutList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setBackgroundColor();

        aboutList = (ListView) findViewById(R.id.list_about);
        aboutList.setAdapter(new AboutAdapter(this));
        aboutList.setOnItemClickListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setTheme() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String themeColor = sharedPrefs.getString("theme_color", "5");
        String theme = sharedPrefs.getString("theme","1");
        switch (themeColor) {
            case "1":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Red_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Red);
                }
                break;
            case "2":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Pink_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Pink);
                }
                break;
            case "3":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Purple_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Purple);
                }
                break;
            case "4":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_DeepPurple_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_DeepPurple);
                }
                break;
            case "5":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Indigo_Dark);
                }
                break;
            case "6":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Blue_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Blue);
                }
                break;
            case "7":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_LightBlue_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_LightBlue);
                }
                break;
            case "8":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Cyan_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Cyan);
                }
                break;
            case "9":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Teal_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Teal);
                }
                break;
            case "10":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Green_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Green);
                }
                break;
            case "11":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_LightGreen_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_LightGreen);
                }
                break;
            case "12":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Lime_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Lime);
                }
                break;
            case "13":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Yellow_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Yellow);
                }
                break;
            case "14":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Amber_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Amber);
                }
                break;
            case "15":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Orange_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Orange);
                }
                break;
            case "16":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_DeepOrange_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_DeepOrange);
                }
                break;
            case "17":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Brown_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Brown);
                }
                break;
            case "18":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Grey_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Grey);
                }
                break;
            case "19":
                if (theme.equals("2")) {
                }
                else {
                    setTheme(R.style.AppTheme_BlueGrey);
                }
                break;
        }
    }

    private void setBackgroundColor() {
        LinearLayout header = (LinearLayout) findViewById(R.id.header);

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String themeColor = sharedPrefs.getString("theme_color", "5");
        switch (themeColor) {
            case "1":
                header.setBackgroundColor(Color.parseColor("#F44336"));
                break;
            case "2":
                header.setBackgroundColor(Color.parseColor("#E91E63"));
                break;
            case "3":
                header.setBackgroundColor(Color.parseColor("#9C27B0"));
                break;
            case "4":
                header.setBackgroundColor(Color.parseColor("#673AB7"));
                break;
            case "5":
                break;
            case "6":
                header.setBackgroundColor(Color.parseColor("#2196F3"));
                break;
            case "7":
                header.setBackgroundColor(Color.parseColor("#03A9F4"));
                break;
            case "8":
                header.setBackgroundColor(Color.parseColor("#00BCD4"));
                break;
            case "9":
                header.setBackgroundColor(Color.parseColor("#009688"));
                break;
            case "10":
                header.setBackgroundColor(Color.parseColor("#4CAF50"));
                break;
            case "11":
                header.setBackgroundColor(Color.parseColor("#8BC34A"));
                break;
            case "12":
                header.setBackgroundColor(Color.parseColor("#CDDC39"));
                break;
            case "13":
                header.setBackgroundColor(Color.parseColor("#FFEB3B"));
                break;
            case "14":
                header.setBackgroundColor(Color.parseColor("#FFC107"));
                break;
            case "15":
                header.setBackgroundColor(Color.parseColor("#FF9800"));
                break;
            case "16":
                header.setBackgroundColor(Color.parseColor("#FF5722"));
                break;
            case "17":
                header.setBackgroundColor(Color.parseColor("#795548"));
                break;
            case "18":
                header.setBackgroundColor(Color.parseColor("#9E9E9E"));
                break;
            case "19":
                header.setBackgroundColor(Color.parseColor("#607D8B"));
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == 2) {
            showChangelogDialog();
        }
        else if (position == 3) {
            showCreditsDialog();
        }
        else if (position == 4) {
            openGooglePlusCommunity();
        }
        else if (position == 5) {
            rateApp();
        }
    }

    private void rateApp() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        }
        catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void openGooglePlusCommunity() {
        String googlePlusURL = "https://plus.google.com/communities/104504160658515569476";
        Intent googlePlus = new Intent(Intent.ACTION_VIEW, Uri.parse(googlePlusURL));
        startActivity(googlePlus);
    }

    private void showChangelogDialog() {
        FragmentManager manager = getFragmentManager();
        ChangelogDialogFragment changelogDialogFragment = new ChangelogDialogFragment();
        changelogDialogFragment.show(manager, "changelog");
    }

    private void showCreditsDialog() {
        FragmentManager manager = getFragmentManager();
        CreditsDialogFragment creditsDialogFragment = new CreditsDialogFragment();
        creditsDialogFragment.show(manager, "credits");
    }
}

class SingleRow {
    String title;
    String description;
    SingleRow(String title, String description) {
        this.title = title;
        this.description = description;
    }
}
class AboutAdapter extends BaseAdapter {

    ArrayList<SingleRow> aboutList;
    Context context;
    AboutAdapter(Context c) {
        context = c;
        aboutList = new ArrayList<SingleRow>();
        Resources resources = c.getResources();
        String[] titles = resources.getStringArray(R.array.titles);
        String[] descriptions = resources.getStringArray(R.array.descriptions);
        for (int position = 0; position < 6; position++) {
            aboutList.add(new SingleRow(titles[position], descriptions[position]));
        }

    }

    @Override
    public int getCount() {
        return aboutList.size();
    }

    @Override
    public Object getItem(int position) {
        return aboutList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = layoutInflater.inflate(R.layout.single_row_about, parent, false);
        TextView title = (TextView) row.findViewById(R.id.textTitle);
        TextView description = (TextView) row.findViewById(R.id.textDescription);

        SingleRow temp = aboutList.get(position);

        title.setText(temp.title);
        description.setText(temp.description);

        return row;
    }
}
