package cz.tedsoft.tipcalculator;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.wearable.view.WatchViewStub;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SplitActivity extends Activity {

    private Button button1,button2,button3,button4,button5,button6,button7,button8,button9,button0,buttonNext,buttonDot,buttonDelete;
    private EditText editSplit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split);

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        final String defaultSplit = sharedPrefs.getString("default_split", "");

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                editSplit = (EditText) stub.findViewById(R.id.editSplit);
                if (!defaultSplit.equals("")) {
                    editSplit.setText(defaultSplit);
                }
                button1 = (Button) stub.findViewById(R.id.button1);
                button1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editSplit.append("1");
                    }
                });
                button2 = (Button) findViewById(R.id.button2);
                button2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editSplit.append("2");
                    }
                });
                button3 = (Button) stub.findViewById(R.id.button3);
                button3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editSplit.append("3");
                    }
                });
                button4 = (Button) stub.findViewById(R.id.button4);
                button4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editSplit.append("4");
                    }
                });
                button5 = (Button) stub.findViewById(R.id.button5);
                button5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editSplit.append("5");
                    }
                });
                button6 = (Button) stub.findViewById(R.id.button6);
                button6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editSplit.append("6");
                    }
                });
                button7 = (Button) stub.findViewById(R.id.button7);
                button7.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editSplit.append("7");
                    }
                });
                button8 = (Button) stub.findViewById(R.id.button8);
                button8.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editSplit.append("8");
                    }
                });
                button9 = (Button) stub.findViewById(R.id.button9);
                button9.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editSplit.append("9");
                    }
                });
                button0 = (Button) stub.findViewById(R.id.button0);
                button0.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editSplit.append("0");
                    }
                });
                buttonNext = (Button) stub.findViewById(R.id.buttonNext);
                buttonNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startResultsActivity();
                    }
                });
                buttonDot = (Button) stub.findViewById(R.id.buttonDot);
                buttonDot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editSplit.append(".");
                    }
                });
                buttonDelete = (Button) stub.findViewById(R.id.buttonDelete);
                buttonDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int length = editSplit.getText().length();
                        if (length > 0) {
                            editSplit.getText().delete(length - 1, length);
                        }
                    }
                });
            }
        });
    }

    private void startResultsActivity () {
        String subtotalInput = getIntent().getExtras().getString("subtotal");
        String tipPercentageInput = getIntent().getExtras().getString("tip");
        String splitInput = editSplit.getText().toString();
        if(!TextUtils.isEmpty(splitInput)) {
            Intent intent = new Intent(this, ResultsActivity.class);
            intent.putExtra("subtotal", subtotalInput);
            intent.putExtra("tip", tipPercentageInput);
            intent.putExtra("split", splitInput);
            startActivity(intent);
        }
    }
}
