package cz.tedsoft.tipcalculator;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;


public class SettingsFragment extends PreferenceFragment {

    ListPreference theme, themeColor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);

        disableSettings();
    }

    private void disableSettings() {
        theme = (ListPreference) findPreference("theme");
        themeColor = (ListPreference) findPreference("theme_color");
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean proVersion = sharedPrefs.getBoolean("pro", false);
        if (!proVersion) {
            theme.setEnabled(false);
            themeColor.setEnabled(false);
        }
    }
}