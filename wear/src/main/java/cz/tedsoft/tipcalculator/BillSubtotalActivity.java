package cz.tedsoft.tipcalculator;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.wearable.view.WatchViewStub;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class BillSubtotalActivity extends Activity {

    private Button button1,button2,button3,button4,button5,button6,button7,button8,button9,button0,buttonNext,buttonDot,buttonDelete;
    private EditText editBillSubtotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        final boolean proVersion = sharedPrefs.getBoolean("pro", false);
        if (proVersion) {
            setContentView(R.layout.activity_bill_subtotal);
        }
        else {
            setContentView(R.layout.activity_bill_subtotal_free);
        }
        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                if (proVersion) {
                        editBillSubtotal = (EditText) stub.findViewById(R.id.editBillSubtotal);
                        button1 = (Button) stub.findViewById(R.id.button1);
                        button1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editBillSubtotal.append("1");
                            }
                        });
                        button2 = (Button) findViewById(R.id.button2);
                        button2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editBillSubtotal.append("2");
                            }
                        });
                        button3 = (Button) stub.findViewById(R.id.button3);
                        button3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editBillSubtotal.append("3");
                            }
                        });
                        button4 = (Button) stub.findViewById(R.id.button4);
                        button4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editBillSubtotal.append("4");
                            }
                        });
                        button5 = (Button) stub.findViewById(R.id.button5);
                        button5.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editBillSubtotal.append("5");
                            }
                        });
                        button6 = (Button) stub.findViewById(R.id.button6);
                        button6.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editBillSubtotal.append("6");
                            }
                        });
                        button7 = (Button) stub.findViewById(R.id.button7);
                        button7.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editBillSubtotal.append("7");
                            }
                        });
                        button8 = (Button) stub.findViewById(R.id.button8);
                        button8.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editBillSubtotal.append("8");
                            }
                        });
                        button9 = (Button) stub.findViewById(R.id.button9);
                        button9.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editBillSubtotal.append("9");
                            }
                        });
                        button0 = (Button) stub.findViewById(R.id.button0);
                        button0.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editBillSubtotal.append("0");
                            }
                        });
                        buttonNext = (Button) stub.findViewById(R.id.buttonNext);
                        buttonNext.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                startTipPercentageActivity();
                            }
                        });
                        buttonDot = (Button) stub.findViewById(R.id.buttonDot);
                        buttonDot.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                editBillSubtotal.append(".");
                            }
                        });
                        buttonDelete = (Button) stub.findViewById(R.id.buttonDelete);
                        buttonDelete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                int length = editBillSubtotal.getText().length();
                                if (length > 0) {
                                    editBillSubtotal.getText().delete(length - 1, length);
                                }
                            }
                        });
                }
            }
        });
    }

    private void startTipPercentageActivity () {
        String subtotalInput = editBillSubtotal.getText().toString();
        if(!TextUtils.isEmpty(subtotalInput)) {
            Intent intent = new Intent(this, TipPercentageActivity.class);
            intent.putExtra("subtotal", subtotalInput);
            startActivity(intent);
        }
    }
}
