package cz.tedsoft.tipcalculator;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewAnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.Wearable;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import cz.tedsoft.tipcalculator.util.IabHelper;
import cz.tedsoft.tipcalculator.util.IabResult;
import cz.tedsoft.tipcalculator.util.Inventory;
import cz.tedsoft.tipcalculator.util.Purchase;
import hotchemi.android.rate.AppRate;
import hotchemi.android.rate.OnClickButtonListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textBillTotalAmount,textBillTotalPerPersonAmount,textTipTotalAmount,textTipTotalPerPersonAmount,textBillSubtotal;
    private EditText editBillSubtotal,editTip,editSplit;

    GoogleApiClient googleApiClient;

    IabHelper helper;
    static final String SKU_PRO = "cz.tedsoft.tipcalculator.pro";
    boolean isPro = false;
    static final int RC_REQUEST = 10001;
    static final String token = "tip_calculator_pro_token";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean firstLaunch = sharedPrefs.getBoolean("first_launch", true);
        if (firstLaunch) {
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putBoolean("first_launch", false);
            editor.apply();
            Intent intent = new Intent(this, TipCalculatorIntro.class);
            startActivity(intent);
        }

        boolean proVersion = sharedPrefs.getBoolean("pro", false);
        if (proVersion) {
            setTheme(sharedPrefs);
        }
        super.onCreate(savedInstanceState);
        checkProVersion(proVersion);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        initVisibility();
        setSupportActionBar(toolbar);
        if (proVersion) {
            setBackgroundColor();
        }
        googleApiSetup();
        inAppPurchaseSetup();
        init();
    }

    private void initVisibility() {
        View viewFab = findViewById(R.id.fab);
        View viewFabClear = findViewById(R.id.fabClear);
        viewFabClear.setVisibility(View.GONE);
        viewFab.setVisibility(View.VISIBLE);
    }

    private void googleApiSetup() {
        googleApiClient = new GoogleApiClient.Builder(getBaseContext())
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                    }
                })
                .addApi(Wearable.API)
                .build();
        googleApiClient.connect();
    }

    private void checkProVersion(boolean proVersion) {
        if (!proVersion) {
            setContentView(R.layout.activity_main_ads);
            AdView mAdView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
       }
        else {
            setContentView(R.layout.activity_main);
        }
    }

    private void inAppPurchaseSetup() {
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv5dEwOGVDVlwiFNKaStBc6rPuhv1jWln7nxootzTTFYjzhTz1QiuQ0tFHaGouNHUcniHvUlGQLTK3r/T93DM7y8dXNjW5bh7MZDEfR5EhR+CUXh30ZLk+RR0wYX8GvipIdfUf6gn1XLoVz0g1q9+gdGwzcnZ8jhcfjgFK6jWyoBc5qEnjbLmaBRi7b6/SmuBdq0oi4gnltg0qw7i6D2OVxBU1CN8BULiGvVpJJ9bZkm6uQMGs7+IZVhC9hqWj1LUSfqw+1fMFV0UvaaTYp5A/7ewEnFwHtWOmLlek1xln1vXbdv33UCCK+Ek5tiW9Zxz0ZoIHF9RostKMGI9jmKy6QIDAQAB";
        helper = new IabHelper(this, base64EncodedPublicKey);
        helper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (helper == null) {
                    return;
                }
                if (result.isSuccess()) {
                    helper.queryInventoryAsync(gotInventoryListener);
                }
                else {
                    return;
                }
            }
        });
    }

    IabHelper.QueryInventoryFinishedListener gotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if (helper == null) {
                return;
            }

            Purchase proPurchase = inventory.getPurchase(SKU_PRO);
            isPro = (proPurchase != null);

            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
            SharedPreferences.Editor editor = sharedPrefs.edit();

            if (isPro) {
                editor.putBoolean("pro", true);
                editor.apply();

                wearProVersion();
            }
            else {
                editor.putBoolean("pro", false);
                editor.apply();
            }
        }
    };

    private void wearProVersion() {
        if(googleApiClient==null)
            return;

        final PutDataMapRequest putRequest = PutDataMapRequest.create("/Wear_settings");
        final DataMap map = putRequest.getDataMap();
        map.putBoolean("pro", isPro);
        Wearable.DataApi.putDataItem(googleApiClient, putRequest.asPutDataRequest());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (helper == null) {
            return;
        }

        // Pass on the activity result to the helper for handling
        if (!helper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }

        else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(getApplicationContext(), R.string.toast_cancelled, Toast.LENGTH_SHORT).show();
        }
        else if (resultCode == IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED) {
            Toast.makeText(getApplicationContext(), R.string.toast_purchased, Toast.LENGTH_SHORT).show();
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (helper == null) {
                return;
            }

            if (!result.isFailure()) {
                if (purchase.getSku().equals(SKU_PRO)) {
                    SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putBoolean("pro", true);
                    editor.apply();

                    wearProVersion();
                }
            }
        }
    };

    public void onDestroy() {
        super.onDestroy();

        if (helper != null) {
            helper.dispose();
            helper = null;
        }
    }

    private void setTheme(SharedPreferences sharedPrefs) {
        String themeColor = sharedPrefs.getString("theme_color", "5");
        String theme = sharedPrefs.getString("theme","1");
        switch (themeColor) {
            case "1":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Red_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Red);
                }
                break;
            case "2":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Pink_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Pink);
                }
                break;
            case "3":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Purple_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Purple);
                }
                break;
            case "4":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_DeepPurple_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_DeepPurple);
                }
                break;
            case "5":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Indigo_Dark);
                }
                break;
            case "6":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Blue_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Blue);
                }
                break;
            case "7":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_LightBlue_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_LightBlue);
                }
                break;
            case "8":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Cyan_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Cyan);
                }
                break;
            case "9":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Teal_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Teal);
                }
                break;
            case "10":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Green_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Green);
                }
                break;
            case "11":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_LightGreen_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_LightGreen);
                }
                break;
            case "12":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Lime_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Lime);
                }
                break;
            case "13":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Yellow_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Yellow);
                }
                break;
            case "14":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Amber_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Amber);
                }
                break;
            case "15":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Orange_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Orange);
                }
                break;
            case "16":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_DeepOrange_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_DeepOrange);
                }
                break;
            case "17":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Brown_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Brown);
                }
                break;
            case "18":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_Grey_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_Grey);
                }
                break;
            case "19":
                if (theme.equals("2")) {
                    setTheme(R.style.AppTheme_BlueGrey_Dark);
                }
                else {
                    setTheme(R.style.AppTheme_BlueGrey);
                }
                break;
        }
    }

    private void setBackgroundColor() {
        LinearLayout viewTop = (LinearLayout) findViewById(R.id.viewTop);

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String themeColor = sharedPrefs.getString("theme_color", "5");
        switch (themeColor) {
            case "1":
                viewTop.setBackgroundColor(Color.parseColor("#F44336"));
                break;
            case "2":
                viewTop.setBackgroundColor(Color.parseColor("#E91E63"));
                break;
            case "3":
                viewTop.setBackgroundColor(Color.parseColor("#9C27B0"));
                break;
            case "4":
                viewTop.setBackgroundColor(Color.parseColor("#673AB7"));
                break;
            case "5":
                break;
            case "6":
                viewTop.setBackgroundColor(Color.parseColor("#2196F3"));
                break;
            case "7":
                viewTop.setBackgroundColor(Color.parseColor("#03A9F4"));
                break;
            case "8":
                viewTop.setBackgroundColor(Color.parseColor("#00BCD4"));
                break;
            case "9":
                viewTop.setBackgroundColor(Color.parseColor("#009688"));
                break;
            case "10":
                viewTop.setBackgroundColor(Color.parseColor("#4CAF50"));
                break;
            case "11":
                viewTop.setBackgroundColor(Color.parseColor("#8BC34A"));
                break;
            case "12":
                viewTop.setBackgroundColor(Color.parseColor("#CDDC39"));
                break;
            case "13":
                viewTop.setBackgroundColor(Color.parseColor("#FFEB3B"));
                break;
            case "14":
                viewTop.setBackgroundColor(Color.parseColor("#FFC107"));
                break;
            case "15":
                viewTop.setBackgroundColor(Color.parseColor("#FF9800"));
                break;
            case "16":
                viewTop.setBackgroundColor(Color.parseColor("#FF5722"));
                break;
            case "17":
                viewTop.setBackgroundColor(Color.parseColor("#795548"));
                break;
            case "18":
                viewTop.setBackgroundColor(Color.parseColor("#9E9E9E"));
                break;
            case "19":
                viewTop.setBackgroundColor(Color.parseColor("#607D8B"));
                break;
        }
    }

    private void init() {
        rateApp();

        editBillSubtotal = (EditText) findViewById(R.id.editBillSubtotal);
        editTip = (EditText) findViewById(R.id.editTip);
        editSplit = (EditText) findViewById(R.id.editSplit);
        textBillTotalAmount = (TextView) findViewById(R.id.textBillTotalAmount);
        textBillTotalPerPersonAmount = (TextView) findViewById(R.id.textBillTotalPerPersonAmount);
        textTipTotalAmount = (TextView) findViewById(R.id.textTipTotalAmount);
        textTipTotalPerPersonAmount = (TextView) findViewById(R.id.textTipTotalPerPersonAmount);
        textBillSubtotal = (TextView) findViewById(R.id.textBillSubtotal);

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String defaultTip = sharedPrefs.getString("default_tip", "");
        String defaultSplit = sharedPrefs.getString("default_split", "");
        String currency = sharedPrefs.getString("currency_value", "");
        String billText = getResources().getString(R.string.bill_subtotal);
        textBillSubtotal.setText(billText + " (" + currency + ")");

        if (!defaultTip.equals("")) {
            editTip.setText(defaultTip);
        }
        if (!defaultSplit.equals("")) {
            editSplit.setText(defaultSplit);
        }

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);

        final FloatingActionButton fabClear = (FloatingActionButton) findViewById(R.id.fabClear);
        fabClear.setOnClickListener(this);

        editSplit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    fab.performClick();
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    handled = true;
                }
                return handled;
            }

        });

    }

    private void rateApp() {
        AppRate.with(this)
                .setInstallDays(0)
                .setLaunchTimes(3)
                .setOnClickButtonListener(new OnClickButtonListener() {
                    @Override
                    public void onClickButton(int which) {
                        Log.d(MainActivity.class.getName(),
                        Integer.toString(which));
                    }
                })
                .monitor();
        AppRate.showRateDialogIfMeetsConditions(this);
    }

    @Override
    public void onClick(View view) {
        String subtotalInput = editBillSubtotal.getText().toString();
        String tipPercentageInput = editTip.getText().toString();
        String splitInput = editSplit.getText().toString();
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean roundUp = sharedPrefs.getBoolean("round_up", true);
        String currency = sharedPrefs.getString("currency_value", "");
        String currencyPosition = sharedPrefs.getString("currency_position", "2");
        if (currencyPosition.equals("2")) {
            currency = " " + currency;
        }
        if(!TextUtils.isEmpty(subtotalInput) && !TextUtils.isEmpty(tipPercentageInput) && !TextUtils.isEmpty(splitInput)){
            double subtotal = Double.parseDouble(subtotalInput);
            double tipPercentage = Double.parseDouble(tipPercentageInput);
            int split = Integer.parseInt(splitInput);
            switch(view.getId()) {
                case R.id.fab:
                    if (split == 0) {
                        Snackbar.make(view, R.string.zero_snackbar, Snackbar.LENGTH_SHORT)
                                .setAction("Action", null).show();
                        break;
                    }
                    double tipAmount = subtotal * tipPercentage * 0.01;
                    double tipAmountPerPerson = tipAmount / split;
                    double totalAmount = subtotal + tipAmount;
                    double totalAmountPerPerson = totalAmount / split;

                    NumberFormat nf = new DecimalFormat("#.00");

                    if (roundUp) {
                        double totalAmountRoundUp = Math.ceil(totalAmount);
                        double totalAmountPerPersonRoundUp = totalAmountRoundUp / split;
                        double tipAmountRoundUp = totalAmountRoundUp - subtotal;
                        double tipAmountPerPersonRoundUp = tipAmountRoundUp / split;

                        if (currencyPosition.equals("2")) {
                            textBillTotalAmount.setText(String.valueOf(nf.format(totalAmountRoundUp)) + currency);
                            textBillTotalPerPersonAmount.setText(String.valueOf(nf.format(totalAmountPerPersonRoundUp)) + currency);
                            textTipTotalAmount.setText(String.valueOf(nf.format(tipAmountRoundUp)) + currency);
                            textTipTotalPerPersonAmount.setText(String.valueOf(nf.format(tipAmountPerPersonRoundUp)) + currency);
                        }
                        else if (currencyPosition.equals("1")) {
                            textBillTotalAmount.setText(currency + String.valueOf(nf.format(totalAmountRoundUp)));
                            textBillTotalPerPersonAmount.setText(currency + String.valueOf(nf.format(totalAmountPerPersonRoundUp)));
                            textTipTotalAmount.setText(currency + String.valueOf(nf.format(tipAmountRoundUp)));
                            textTipTotalPerPersonAmount.setText(currency + String.valueOf(nf.format(tipAmountPerPersonRoundUp)));
                        }
                    }
                    else {
                        if (currencyPosition.equals("2")) {
                            textBillTotalAmount.setText(String.valueOf(nf.format(totalAmount)) + currency);
                            textBillTotalPerPersonAmount.setText(String.valueOf(nf.format(totalAmountPerPerson)) + currency);
                            textTipTotalAmount.setText(String.valueOf(nf.format(tipAmount)) + currency);
                            textTipTotalPerPersonAmount.setText(String.valueOf(nf.format(tipAmountPerPerson)) + currency);
                        }
                        else if (currencyPosition.equals("1")) {
                            textBillTotalAmount.setText(currency + String.valueOf(nf.format(totalAmount)));
                            textBillTotalPerPersonAmount.setText(currency + String.valueOf(nf.format(totalAmountPerPerson)));
                            textTipTotalAmount.setText(currency + String.valueOf(nf.format(tipAmount)));
                            textTipTotalPerPersonAmount.setText(currency + String.valueOf(nf.format(tipAmountPerPerson)));
                        }

                    }

                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                    showClearFab();

                    editTextChange();

                    break;
            }
        }
        else {
            Snackbar.make(view, R.string.empty_snackbar, Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
        }
        switch(view.getId()) {
            case R.id.fabClear:
                String defaultTip = sharedPrefs.getString("default_tip", "");
                String defaultSplit = sharedPrefs.getString("default_split", "");

                if (!defaultTip.equals("")) {
                    editTip.setText(defaultTip);
                }
                else {
                    editTip.setText("");
                }

                if (!defaultSplit.equals("")) {
                    editSplit.setText(defaultSplit);
                }
                else {
                    editSplit.setText("");
                }

                editBillSubtotal.setText("");
                textBillTotalAmount.setText("");
                textBillTotalPerPersonAmount.setText("");
                textTipTotalAmount.setText("");
                textTipTotalPerPersonAmount.setText("");

                hideClearFab();
        }
    }

    private void showClearFab() {
        View viewFab = findViewById(R.id.fab);
        View viewFabClear = findViewById(R.id.fabClear);

        viewFab.setVisibility(View.GONE);
        viewFabClear.setVisibility(View.VISIBLE);
    }

    private void hideClearFab() {
        View viewFab = findViewById(R.id.fab);
        View viewFabClear = findViewById(R.id.fabClear);

        viewFabClear.setVisibility(View.GONE);
        viewFab.setVisibility(View.VISIBLE);
    }

    private void editTextChange() {
        editBillSubtotal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                hideClearFab();
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        editTip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                hideClearFab();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        editSplit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                hideClearFab();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        if (id == R.id.action_whats_new) {
            FragmentManager manager = getFragmentManager();
            ChangelogDialogFragment changelogDialogFragment = new ChangelogDialogFragment();
            changelogDialogFragment.show(manager, "changelog");
            return true;
        }

        if (id == R.id.action_buy_app) {
            helper.launchPurchaseFlow(this, SKU_PRO, RC_REQUEST, mPurchaseFinishedListener, token);
            return true;
        }

        if (id == R.id.action_rate_app) {
            final String appPackageName = getPackageName();
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            }
            catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
            return true;
        }

        if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
