package cz.tedsoft.tipcalculator;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.wearable.view.WatchViewStub;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class ResultsActivity extends Activity {

    private TextView textBillTotalAmount,textBillTotalPerPersonAmount,textTipTotalAmount,textTipTotalPerPersonAmount;
    private Button buttonExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        final String subtotalInput = getIntent().getExtras().getString("subtotal");
        final String tipPercentageInput = getIntent().getExtras().getString("tip");
        final String splitInput = getIntent().getExtras().getString("split");

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                buttonExit = (Button) stub.findViewById(R.id.buttonExit);
                buttonExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent startMain = new Intent(Intent.ACTION_MAIN);
                        startMain.addCategory(Intent.CATEGORY_HOME);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);
                        finish();
                    }
                });

                textBillTotalAmount = (TextView) findViewById(R.id.textBillTotalAmount);
                textBillTotalPerPersonAmount = (TextView) findViewById(R.id.textBillTotalPerPersonAmount);
                textTipTotalAmount = (TextView) findViewById(R.id.textTipTotalAmount);
                textTipTotalPerPersonAmount = (TextView) findViewById(R.id.textTipTotalPerPersonAmount);

                double subtotal = Double.parseDouble(subtotalInput);
                double tipPercentage = Double.parseDouble(tipPercentageInput);
                int split = Integer.parseInt(splitInput);

                double tipAmount = subtotal * tipPercentage * 0.01;
                double tipAmountPerPerson = tipAmount / split;
                double totalAmount = subtotal + tipAmount;
                double totalAmountPerPerson = totalAmount / split;

                NumberFormat nf = new DecimalFormat("#.00");

                SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                boolean roundUp = sharedPrefs.getBoolean("round_up", true);
                String currency = sharedPrefs.getString("currency_value", "");
                String currencyPosition = sharedPrefs.getString("currency_position", "2");
                if (currencyPosition.equals("2")) {
                    currency = " " + currency;
                }

                if (roundUp) {
                    double totalAmountRoundUp = Math.ceil(totalAmount);
                    double totalAmountPerPersonRoundUp = totalAmountRoundUp / split;
                    double tipAmountRoundUp = totalAmountRoundUp - subtotal;
                    double tipAmountPerPersonRoundUp = tipAmountRoundUp / split;

                    if (currencyPosition.equals("2")) {
                        textBillTotalAmount.setText(String.valueOf(nf.format(totalAmountRoundUp)) + currency);
                        textBillTotalPerPersonAmount.setText(String.valueOf(nf.format(totalAmountPerPersonRoundUp)) + currency);
                        textTipTotalAmount.setText(String.valueOf(nf.format(tipAmountRoundUp)) + currency);
                        textTipTotalPerPersonAmount.setText(String.valueOf(nf.format(tipAmountPerPersonRoundUp)) + currency);
                    }
                    else if (currencyPosition.equals("1")) {
                        textBillTotalAmount.setText(currency + String.valueOf(nf.format(totalAmountRoundUp)));
                        textBillTotalPerPersonAmount.setText(currency + String.valueOf(nf.format(totalAmountPerPersonRoundUp)));
                        textTipTotalAmount.setText(currency + String.valueOf(nf.format(tipAmountRoundUp)));
                        textTipTotalPerPersonAmount.setText(currency + String.valueOf(nf.format(tipAmountPerPersonRoundUp)));
                    }
                }
                else {
                    if (currencyPosition.equals("2")) {
                        textBillTotalAmount.setText(String.valueOf(nf.format(totalAmount)) + currency);
                        textBillTotalPerPersonAmount.setText(String.valueOf(nf.format(totalAmountPerPerson)) + currency);
                        textTipTotalAmount.setText(String.valueOf(nf.format(tipAmount)) + currency);
                        textTipTotalPerPersonAmount.setText(String.valueOf(nf.format(tipAmountPerPerson)) + currency);
                    }
                    else if (currencyPosition.equals("1")) {
                        textBillTotalAmount.setText(currency + String.valueOf(nf.format(totalAmount)));
                        textBillTotalPerPersonAmount.setText(currency + String.valueOf(nf.format(totalAmountPerPerson)));
                        textTipTotalAmount.setText(currency + String.valueOf(nf.format(tipAmount)));
                        textTipTotalPerPersonAmount.setText(currency + String.valueOf(nf.format(tipAmountPerPerson)));
                    }

                }
            }
        });
    }
}
